﻿using System;
using System.Windows.Forms;
using SMS_Broadcast_Message_Manager_v2;
using System.ServiceProcess;

namespace SMS_Broadcast_Message_Manager_v2
{
    static class SMPP_Interface
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        
        static void Main()
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            log4net.Config.XmlConfigurator.Configure();
#if DEBUG
            Broadcaster the_broadcast = new Broadcaster();
            the_broadcast.start();
            System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
#else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new Broadcaster()
            };
            ServiceBase.Run(ServicesToRun);
#endif
        }
    }
}
