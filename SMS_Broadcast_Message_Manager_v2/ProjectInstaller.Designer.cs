﻿namespace SMS_Broadcast_Message_Manager_v2
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SMS_Broadcaster_Process_Installer = new System.ServiceProcess.ServiceProcessInstaller();
            this.SMS_Broadcaster_Service_Installer = new System.ServiceProcess.ServiceInstaller();
            // 
            // SMS_Broadcaster_Process_Installer
            // 
            this.SMS_Broadcaster_Process_Installer.Password = null;
            this.SMS_Broadcaster_Process_Installer.Username = null;
            this.SMS_Broadcaster_Process_Installer.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.SMS_Broadcaster_Process_Installer_AfterInstall);
            // 
            // SMS_Broadcaster_Service_Installer
            // 
            this.SMS_Broadcaster_Service_Installer.ServiceName = "SMS_Broadcaster";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.SMS_Broadcaster_Process_Installer,
            this.SMS_Broadcaster_Service_Installer});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller SMS_Broadcaster_Process_Installer;
        private System.ServiceProcess.ServiceInstaller SMS_Broadcaster_Service_Installer;
    }
}