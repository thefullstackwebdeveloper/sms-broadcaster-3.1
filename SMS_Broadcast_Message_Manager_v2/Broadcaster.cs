﻿using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Timers;
using SMS_Broadcast_Message_Manager_v2.HelperClass;

namespace SMS_Broadcast_Message_Manager_v2
{
    public partial class Broadcaster : ServiceBase
    {
        static Database db = new Database();
        static SMSPP_Interface smpp = new SMSPP_Interface();
        static Broadcast_Message[] messages;
        static Setting default_settings;
        static Timer sycher;
        static int counter = 0;

        // Cycle duration in miliseconds
        const int cycle_period = 60000;

        public Broadcaster()
        {
            this.ServiceName = "Broadcaster";
        }

        public void onDebug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {

            start(cycle_period);
        }

        protected override void OnStop()
        {}

        public static void CheckAndSend(Object source, ElapsedEventArgs e)
        {
            Debug.Write("Check and Send invoked\n");
            messages = db.get_current_messages();
            try
            { 
                foreach (Broadcast_Message message in messages)
                {

                    EventLog.WriteEntry("Broadcaster", "Current Date & Time :" + e.SignalTime +  "\n");
                    Debug.Write("Current Date :" + e.SignalTime + "\n");
                    Debug.Write("Message sender : " + message.Source + "  " + "Message receiver : " + message.Destination + "\n");
                    Debug.Write("Message Date_Send :" + Convert.ToDateTime(message.Date_Time_Send) + "\n");

                    if (Convert.ToDateTime(message.Date_Time_Send) > e.SignalTime)
                    {
                        counter--;
                    }
                    else if (Convert.ToDateTime(message.Date_Time_Send) <= e.SignalTime && message.submitted.Trim().ToUpper() != "TRUE")                   
                    {
                        Debug.Write("Send message\n");
                        Debug.Write("Current Date :" + e.SignalTime.Date + " Current Time :" + e.SignalTime.ToString("HH:mm") + "\n");
                        Debug.Write("Message Sent Date_Send :" + Convert.ToDateTime(message.Date_Time_Send)); 
                        EventLog.WriteEntry("Broadcaster", "Message Sent :" + e.SignalTime + "\n");
                        default_settings = db.get_settings(message.Settings_ID);
                        smpp.send(message,default_settings);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Write("An exception occurred : " + ex);
            }
        }
               
        public void start(int interval = cycle_period)
        {
            db.connect();
            sycher = new Timer(interval);
            sycher.Elapsed += CheckAndSend;
            sycher.Enabled = true;
            System.Diagnostics.Debug.Write("Timer started\n");
        }

        public void stop()
        {}

        private void InitializeComponent()
        {
            // 
            // Broadcaster
            // 
            this.CanHandlePowerEvent = true;
            this.CanHandleSessionChangeEvent = true;
            this.CanPauseAndContinue = true;
            this.ServiceName = "SMS_Broadcaster";

        }
    }
}
