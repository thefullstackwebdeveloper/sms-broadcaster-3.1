﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;

namespace SMS_Broadcast_Message_Manager_v2
{ 
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }

        private void SMS_Broadcaster_Process_Installer_AfterInstall(object sender, InstallEventArgs e)
        {

        }
    }
}
