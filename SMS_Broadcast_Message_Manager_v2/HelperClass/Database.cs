﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows.Forms;
using SMS_Broadcast_Message_Manager_v2.Properties;


namespace SMS_Broadcast_Message_Manager_v2.HelperClass
{
    public class Database : IDisposable
    {
         SqlConnection connection;
         SqlCommand cmd , cmd2;
         string connection_string;
         log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Database));

        public Database()
        {

        }

        //Intialization settings for the function.

        public void intialize_settings()
        {
            string data_source = (String)Settings.Default["Data_Source"];
            string initial_catalog = (String)Settings.Default["Initial_Catalog"];
            string integrated_security = (String)Settings.Default["Integrated_Security"];

            connection_string = "Data Source=" + data_source +
                             ";Initial Catalog=" + initial_catalog +
                             ";Integrated Security=" + integrated_security + ";";
 
            //MessageBox.Show(connection_string);
            
        }

        //Database connectivity functions

        public Boolean connect()
        {
            intialize_settings();
            try
            {
                connection = new SqlConnection(connection_string);
                connection.Open();
                //MessageBox.Show("Connected to server")
                logger.Info("Connected to database : " + (String)Settings.Default["Data_Source"]);
            }
            catch (Exception e)
            {
                //MessageBox.Show("Couldn't connect, Please review settings : \n" + e);
                logger.Error("Couldn't connect to database, error ocurrred :", e);
                return false;
            }

            
            return true;
            
        }

        public Boolean connected()
        {
            try
            {
                if (connection.State == ConnectionState.Open)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                logger.Error("An exception has occurred : "+e);
                return false;
            }
            
        }

        public Boolean disconnect()
        {
            try
            {
                logger.Info("Disconnected from server");
                //MessageBox.Show("SQL server disconnected", "Disconnected");
            }
            catch (SqlException e)
            {
                logger.Error("An exception has occurred : " + e);
                return false;
            }

            Dispose();
            return true;
            
        }

        //Message retrieval functions

        public Broadcast_Message find_by_message_id(string message_id)
        {
            Broadcast_Message message =  new Broadcast_Message();
            string sql_string = "SELECT ID"
                                + ",Source_Addr"
                                + ",Destination_Addr"
                                + ",Data_Type"
                                + ",Text"
                                + ",Submit_Mode"
                                + ",Date_Time_Send"
                                + ",Sent"
                                + ",Submitted"
                                + ",Message_ID"
                                + " FROM Broadcast_Messages "
                                +" WHERE Message_ID = @Message_id";

            using(cmd =  new SqlCommand(sql_string,connection))
            {

                cmd.Parameters.AddWithValue("@Message_id", message_id);

                SqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        message.ID = (int)reader["ID"];
                        message.Source = reader["Source_Addr"].ToString();
                        message.Destination = reader["Destination_Addr"].ToString();
                        message.Data_Type = reader["Data_Type"].ToString();
                        message.Text = reader["Text"].ToString();
                        message.Submit_Mode = reader["Submit_Mode"].ToString();
                        message.Date_Time_Send = Convert.ToString(reader["Date_Time_Send"]);
                        message.sent = reader["Sent"].ToString();
                        message.submitted = reader["Submitted"].ToString();
                        message.Message_ID = reader["Message_ID"].ToString();
                    }
                    reader.Close();
                }
                catch(Exception e)
                {
                    //MessageBox.Show("" + e, "An error occurred");
                    logger.Error("An error occurred : ", e);
                }
                return message;
            }
           
        }

        public Broadcast_Message[] get_messages(int limit = 0, int offset = 0)
        {
            Broadcast_Message[] all_messages;
            string extended_sql = "";
            string pagination = "";
            if (limit > 0)
            {
                pagination = ",ROW_NUMBER() OVER (ORDER BY Date_Send, Time_Send) AS RowNum";
            }
            string sql_string = @"SELECT ID  
                                 ,Source_Addr
                                 ,Destination_Addr
                                 ,Data_Type
                                 ,Text
                                 ,Submit_Mode
                                 ,Date_Time_Send
                                 ,Date_Time_Edited
                                 ,Sent
                                 ,Submitted
                                 ,Message_ID " + pagination + " FROM Broadcast_Messages";

            if (limit > 0)
            {
                extended_sql = @";WITH Broadcast_Result AS(
                " + sql_string + @"
                 SELECT * FROM Broadcast_Result
                 WHERE RowNum >= @Offset
                 AND RowNum < @Offset + @Limit
                ";
            }

            using (cmd = new SqlCommand(sql_string, connection))
            {
                if (limit > 0)
                {
                    cmd.Parameters.AddWithValue("@Offset", offset);
                    cmd.Parameters.AddWithValue("@Limit", limit);
                }

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    var list = new List<Broadcast_Message>();
                    while (reader.Read())
                    {
                        list.Add(new Broadcast_Message
                        {
                            ID = (int)reader["ID"],
                            Source = reader["Source_Addr"].ToString(),
                            Destination = reader["Destination_Addr"].ToString(),
                            Data_Type = reader["Data_Type"].ToString(),
                            Text = reader["Text"].ToString(),
                            Submit_Mode = reader["Submit_Mode"].ToString(),
                            Date_Time_Send = Convert.ToString(reader["Date_Time_Send"]),
                            Message_ID = reader["Message_ID"].ToString(),
                            sent = reader["Sent"].ToString(),
                            submitted = reader["Submitted"].ToString(),
                            Date_Time_Edited = reader["Date_Time_Edited"].ToString(),
                        });
                    }
                all_messages = list.ToArray();
                reader.Close();
                }
            }
            return all_messages;
        }

        //Message CUD functions

        public Boolean update_Message(Broadcast_Message message_to_update)
        {
            
            //MessageBox.Show("" + message_to_update.Date_Send + " : " + message_to_update.Time_Send + " : " + message_to_update.Date_Time_Edited + " : " + message_to_update.Date_Time_Created);
            string sql_string = "UPDATE Broadcast_Messages "
                                + "SET " 
                                + "Source_Addr = @Source_Addr"
                                + ",Destination_Addr = @Destination_Addr"
                                + ",Data_Type = @Data_Type"
                                + ",Text = @Text"
                                + ",Submit_Mode = @Submit_Mode"
                                + ",Date_Time_Send = @Date_Time_Send"
                                + ",Date_Time_Edited = @Date_Time_Edited"
                                + ",Sent = @Sent"
                                + ",Submitted = @Submitted"
                                + ",Message_ID = @Message_ID "
                                + " WHERE ID = @ID";

            cmd2 = new SqlCommand(sql_string, connection);
            cmd2.Parameters.AddWithValue("@ID", message_to_update.ID);
            cmd2.Parameters.AddWithValue("@Source_Addr", message_to_update.Source);
            cmd2.Parameters.AddWithValue("@Destination_Addr", message_to_update.Destination);
            cmd2.Parameters.AddWithValue("@Data_Type", message_to_update.Data_Type);
            cmd2.Parameters.AddWithValue("@Text", message_to_update.Text);
            cmd2.Parameters.AddWithValue("@Submit_Mode", message_to_update.Submit_Mode);
            cmd2.Parameters.AddWithValue("@Date_Time_Send", Convert.ToDateTime(message_to_update.Date_Time_Send));
            cmd2.Parameters.AddWithValue("@Date_Time_Edited", DateTime.Now);
            cmd2.Parameters.AddWithValue("@Sent", message_to_update.sent);
            cmd2.Parameters.AddWithValue("@Submitted", message_to_update.submitted);
            cmd2.Parameters.AddWithValue("@Message_ID", message_to_update.Message_ID);
           

            try
            {
                cmd2.ExecuteNonQuery();
                logger.Info("Message " + message_to_update.ID + " has been updated");
            }
            catch(Exception e)
            {
                logger.Info("An error occurred : " + e);
              
                return false;
            }
            return true;
        }

        public Boolean delete_Message(int Index)
        {
            string sql_string = "DELETE FROM Broadcast_Messages WHERE ID = @ID";
            cmd = new SqlCommand(sql_string, connection);
            cmd.Parameters.AddWithValue("@ID", Index);
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch(Exception e)
            {
                logger.Error("An exception has occurred : " + e);
                //MessageBox.Show(""+e, "An error occurred");
                return false;
            }

            return true;
        }

        //Settings retrieval functions

        public Setting get_settings(int setting_ID)
        {
            if(setting_ID < 1)
            {
                setting_ID = 1;
            }
            Setting message_settings = new Setting();

            string sql_string = "SELECT System_id"
                               + ",Password"
                               + ",System_type"
                               + ",Hostname"
                               + ",Port"
                               + ",Source_Address_TON"
                               + ",Source_Address_NPI"
                               + ",Destination_Address_TON"
                               + ",Destination_Address_NPI"
                               + ",System_Type"
                               + " FROM SMPP_Settings"
                               + " WHERE Settings_ID = @Settings_ID";

            using (cmd = new SqlCommand(sql_string, connection))
            {
                cmd.Parameters.AddWithValue("@Settings_ID", setting_ID);

                SqlDataReader reader = cmd.ExecuteReader();
                try
                {
                    while (reader.Read())
                    {
                        message_settings.System_id = reader["System_id"].ToString();
                        message_settings.Password = reader["Password"].ToString();
                        message_settings.Hostname = reader["Hostname"].ToString();
                        message_settings.Port = reader["Port"].ToString();
                        message_settings.Source_Address_TON = reader["Source_Address_TON"].ToString();
                        message_settings.Source_Address_NPI = reader["Source_Address_NPI"].ToString();
                        message_settings.Destination_Address_TON = reader["Destination_Address_TON"].ToString();
                        message_settings.Destination_Address_NPI = reader["Destination_Address_NPI"].ToString();
                        message_settings.System_type = reader["System_Type"].ToString();
                    }
                    reader.Close();
                }
                catch (Exception e)
                {
                    logger.Error("An exception has occurred : " + e);
                }
            }

                return message_settings;
        }        

        public void Dispose()
        {

            if(connection != null)
                connection.Dispose();

            if(cmd != null)
                cmd.Dispose();

            if (cmd2 != null)
                 cmd2.Dispose();
        }

        //Optimized function prototype

        public Broadcast_Message[] get_current_messages()
        {
            Broadcast_Message[] the_messages;

            DateTime current = DateTime.Now;

            string sql_string = "SELECT ID "
                               + ",Source_Addr"
                               + ",Destination_Addr"
                               + ",Data_Type"
                               + ",Text"
                               + ",Submit_Mode"
                               + ",Date_Time_Send"
                               + ",Date_Time_Edited"
                               + ",Sent"
                               + ",Submitted"
                               + ",Message_ID"
                               + " FROM Broadcast_Messages"
                               + " WHERE "
                               + "Submitted = 'false' ";

            try { 
            using (cmd = new SqlCommand(sql_string, connection))
            {

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    var list = new List<Broadcast_Message>();
                    while (reader.Read())
                    {
                        list.Add(new Broadcast_Message
                        {
                            ID = (int)reader["ID"],
                            Source = reader["Source_Addr"].ToString(),
                            Destination = reader["Destination_Addr"].ToString(),
                            Data_Type = reader["Data_Type"].ToString(),
                            Text = reader["Text"].ToString(),
                            Submit_Mode = reader["Submit_Mode"].ToString(),
                            Date_Time_Send = Convert.ToString(reader["Date_Time_Send"]),
                            Message_ID = reader["Message_ID"].ToString(),
                            sent = reader["Sent"].ToString(),
                            submitted = reader["Submitted"].ToString(),
                            Date_Time_Edited = reader["Date_Time_Edited"].ToString()
                        });
                    }
                    the_messages = list.ToArray();
                    reader.Close();
                }

            }

            }
            catch(Exception e)
            {
                logger.Error("An error has occurred : " + e);
                the_messages = null;
            }

            return the_messages;

        }

        //Unused functions

        public Boolean add_Message(Broadcast_Message new_message)
        {

            string sql_string = @"INSERT INTO  Broadcast_Messages 
          ( Source_Addr,
            Destination_Addr,
            Data_Type,
            Text,
            Submit_Mode,
            Date_Send,
            Time_Send,
            Date_Time_Created,
            Date_Time_Edited,
            Sent)
          VALUES
          ( @Source_Addr
           ,@Destination_Addr
           ,@Data_Type
           ,@Text
           ,@Submit_Mode
           ,@Date_Send 
           ,@Time_Send 
           ,@Date_Time_Created
           ,@Date_Time_Edited
           ,@Sent)";

            cmd = new SqlCommand(sql_string, connection);
            cmd.Parameters.AddWithValue("@Source_Addr", new_message.Source);
            cmd.Parameters.AddWithValue("@Destination_Addr", new_message.Destination);
            cmd.Parameters.AddWithValue("@Data_Type", new_message.Data_Type);
            cmd.Parameters.AddWithValue("@Text", new_message.Text);
            cmd.Parameters.AddWithValue("@Submit_Mode", new_message.Submit_Mode);
            cmd.Parameters.AddWithValue("@Date_Send", Convert.ToDateTime(new_message.Date_Time_Send));
            cmd.Parameters.AddWithValue("@Date_Time_Created", DateTime.Now);
            cmd.Parameters.AddWithValue("@Date_Time_Edited", DateTime.Now);
            cmd.Parameters.AddWithValue("@Sent", "false");
            ;



            try
            {
                cmd.ExecuteNonQuery();
                //MessageBox.Show("New message added");
            }
            catch (Exception e)
            {
                logger.Error("An exception has occurred : " + e);
                //MessageBox.Show("Reason : " + e ,"Insert failed");
                return false;
            }

            return true;
        }

        public void reconnect()
        {
            disconnect();
            connect();
        }
    }
}
