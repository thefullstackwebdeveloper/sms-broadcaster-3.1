﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SMS_Broadcast_Message_Manager_v2.HelperClass
{
    public class Setting
    {
        public int Settings_ID { get; set; }
        public string System_id { get; set; }
        public string Password { get; set; }
        public string System_type { get; set; }
        public string Hostname { get; set; }
        public string Port { get; set; }
        public string Source_Address_TON { get; set; }
        public string Source_Address_NPI { get; set; }
        public string Destination_Address_TON { get; set; }
        public string Destination_Address_NPI { get; set; }
        public string Service_Type { get; set; }
    }
}
