﻿using System;

namespace SMS_Broadcast_Message_Manager_v2.HelperClass
{
    public class Broadcast_Message
    {
        public int ID { get; set; }
        public string Source { get; set; }
        public string Destination { get; set; }
        public string Data_Type { get; set; }
        public string Message_ID { get; set; }
        public string Text { get; set; }
        public string Submit_Mode { get; set; }
        public string Date_Time_Send { get; set; }
        public string Date_Time_Created { get; set; }
        public string Date_Time_Edited { get; set; }
        public string sent { get; set; }
        public string submitted { get; set; }
        public int  Settings_ID { get; set; }
    }
}
