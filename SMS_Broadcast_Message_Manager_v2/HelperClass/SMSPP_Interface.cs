﻿// SMPP_Interface.cs
// SMPP_Interface.xml

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Inetlab.SMPP;
using Inetlab.SMPP.Common;
using Inetlab.SMPP.PDU;
using SMS_Broadcast_Message_Manager_v2.Properties;
using Inetlab.SMPP.Builders;
using Inetlab.SMPP.Logging;
using log4net;
using System.Threading;

namespace SMS_Broadcast_Message_Manager_v2.HelperClass
{
    /// <summary>
    /// SMPP_Interface contains the functions that will interact with the SMPP
    /// server via InetLab.dll library which serves as an abstraction class for easy
    /// usage on the main C# scheduler.
    /// </summary>
    public class SMSPP_Interface : IDisposable
    {
        //<summary>Store the client object provided by the InetLab library</summary>
        SmppClient _client;

        //<summary>Store the database object for MS SQL database functions</summary>
        Database db;

        Setting the_settings;

        log4net.ILog logger = log4net.LogManager.GetLogger(typeof(SMPP_Interface));

        /// <summary>
        /// The class constructor intialize the SmppClient object and set the default
        /// settings of the SMPP client via library objects.
        /// 
        /// The database object is also intialized for this purpose.
        /// </summary>
        public SMSPP_Interface()
        {
             _client =  new SmppClient();
             _client.evDeliverSm += new DeliverSmEventHandler(client_evDeliverSm);
             _client.evDeliverSmComplete += new DeliverSmEventHandler(client_evDeliverSmConplete);
             _client.evBindComplete += new BindRespEventHandler(client_evBindComplete);
             _client.evUnBind += new UnBindEventHandler(client_evUnbind);
             _client.evAlertNotification += new AlertNotificationEventHandler(client_evAlertNotifications);
             _client.evConnected += new ConnectedEventHandler(client_evConnected);
             _client.evDisconnected +=  new DisconnectedEventHandler(client_evDisconnected);
             _client.evCancelComplete += new CancelSmRespEventHandler(client_evCancelComplete);
             _client.evConnected += new ConnectedEventHandler(client_evConnected);
             _client.evSubmitComplete += new SubmitSmRespEventHandler(client_evSubmitComplete);
             
             _client.evEnquireLink += new EnquireLinkEventHandler(client_evEnquireLink);
             _client.evGenericNack += new GenericNackEventHandler(client_evGenericNack);
             _client.evPduReceiving +=  new PduEventHandler(client_evPduReceiving);
             _client.evPduRejected += new PduEventHandler(client_evPduRejected);
             _client.evPduSending += new PduEventHandler(client_evPduSending);
             _client.Timeout = 60000;
             _client.EnquireInterval = 60;
             _client.NeedEnquireLink = true;

            log4net.Config.XmlConfigurator.Configure();

            db = new Database();
        }

        /// <summary>
        /// This function connects to SMPP client using the settings provided in
        /// the Bind Settings opitons.
        /// </summary>
        /// <returns>Boolean of connect operation status</returns>
        public Boolean connect()
        {

            logger.Info(the_settings.Hostname);
            if (_client.Status == ConnectionStatus.Closed)
            {
                db.connect();
                string server = (String)the_settings.Hostname.Trim();
                int port = Convert.ToInt32(the_settings.Port);

                try
                {
                    _client.Connect(server, port);
                    logger.Info("Connected to server " + server + " , port : " + port);
                }
                catch (Exception e)
                {
                    logger.Error("An exception has occurred : " + e);
                    //MessageBox.Show("" + e, "An error occured");
                }

                if (_client.Status == ConnectionStatus.Open)
                {
                    //MessageBox.Show("connected from connect function");
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                //MessageBox.Show(""+_client.Status,"Connection status");
                return true;
            }
            
        }

        /// <summary>
        /// Intiate and completes a bind to SMPP server.
        /// </summary>
        /// <returns>Boolean of binding operation status</returns>
        public Boolean bind()
        {
            string username = the_settings.System_id.TrimEnd();
            string password = the_settings.Password.TrimEnd();
            logger.Info(username);
            logger.Info(password);

            if (_client.Status == ConnectionStatus.Open && _client.Status != ConnectionStatus.Bound)
            {
                _client.Bind(username, password, ConnectionMode.Transmitter);

                if (_client.Status == ConnectionStatus.Bound)
                {
                   //MessageBox.Show("Bounded in Transmitter mode ", "Bounded");
                   return true;
                }
                else
                { 
                    //MessageBox.Show("Binding could not be done");
                    return false;
                }

             }
            else if(_client.Status == ConnectionStatus.Bound)
            {
                //MessageBox.Show("Already bounded");
                return true;
            }
            else
            { 
                //MessageBox.Show("No connection was established");
                return false;
            }
        }

        /// <summary>
        /// Intiate a disconnect request to SMPP server.
        /// </summary>
        /// <returns> Boolean of disconnection operation result </returns>
        public Boolean disconnect()
        {
            unbind();
            _client.Disconnect();
            //MessageBox.Show("Disconnected");
            //MessageBox.Show(""+(_client.Status == ConnectionStatus.Open));
            db.disconnect();
            return true;
        }

        /// <summary>
        /// Intiate a unbind request to SMPP server 
        /// </summary>
        /// <returns>Boolean of the unbind operation result</returns>
        public Boolean unbind()
        {
            _client.UnBind();
            //MessageBox.Show("Unbinded");
            return true;
        }


        /// <summary>
        /// Sends an PDU to send an SMS specified in the parameter.
        /// </summary>
        /// <param name="message">SMS that are going to be sent</param>
        /// <param name="setting">Settings profile stored in the database</param>
        /// <returns></returns>
        public Boolean send(Broadcast_Message message, Setting settings)
        {
            the_settings = settings;

            _client.AddrNpi = Convert.ToByte(settings.Source_Address_NPI);
            _client.AddrTon = Convert.ToByte(settings.Source_Address_TON);
            _client.SystemType = the_settings.System_type.Trim();


            string sender = message.Source.TrimEnd();
            
            byte senderNPI = Convert.ToByte(Convert.ToInt32(settings.Source_Address_NPI));
            byte senderTON = Convert.ToByte(Convert.ToInt32(settings.Source_Address_TON));

            string receiver = message.Destination.TrimEnd();
            byte receiverNPI = Convert.ToByte(Convert.ToInt32(settings.Destination_Address_NPI));
            byte receiverTON = Convert.ToByte(Convert.ToInt32(settings.Destination_Address_TON));

            DataCodings format = (DataCodings)Enum.Parse(typeof(DataCodings), message.Data_Type);
            SubmitMode submitMode = (SubmitMode)Enum.Parse(typeof(SubmitMode), message.Submit_Mode);


            if (connect() && bind()) 
            {
                String info = "";

                ISubmitSmBuilder builder =
                  SMS.ForSubmit()
                  .From(sender, senderTON, senderNPI)
                  .To(receiver, receiverTON, receiverNPI)
                  .Text(message.Text.Trim())
                  .DeliveryReceipt()
                  .Coding(format)
                  .ServiceType(settings.Service_Type)
                  .ExpireIn(TimeSpan.FromDays(2));
                
                switch (submitMode)
                {
                    case SubmitMode.Payload:
                        builder.MessageInPayload();
                        break;

                    case SubmitMode.ShortMessageWithSAR:
                        builder.ConcatenationInSAR();
                        break;
                }

                IList<SubmitSmResp> respList = _client.Submit(builder);

                if (respList.Count > 0 && respList[0].Status == Inetlab.SMPP.Common.CommandStatus.ESME_ROK)
                {
                    info += "Test message sent \n";
                    foreach (SubmitSmResp resp in respList)
                    {
                        info += "Message ID : " + resp.MessageId + "\n";
                        info += "Status : " + resp.Status + "\n";
                        info += "Sequence : " + resp.Sequence + "\n";
                        info += "Request : " + resp.Request + "\n";
                        message.Message_ID = resp.MessageId;  
                    }
                    logger.Info(info);
                    db.update_Message(message);
                    
                }
                else
                {
                    foreach (SubmitSmResp resp in respList)
                    {
                        info += "Message ID : " + resp.MessageId + "\n";
                        info += "Status : " + resp.Status + "\n";
                        info += "Sequence : " + resp.Sequence + "\n";
                        info += "Request : " + resp.Request + "\n";
                    }
                    logger.Info(info);
                    //MessageBox.Show(info, "Info");

                    return false;
                }
                
            }
            return true;
        }

        /// <summary>
        /// Handles received delivery report and mark sent messages by referring
        /// their message id.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        /// 

        
        /*
            Event functions definitions

        */
        private void client_evDeliverSm(object sender, DeliverSm data)
        {
            Broadcast_Message deliver_message;
            try
            {
                //Check if we received Delivery Receipt
                if (data.MessageType == MessageTypes.SMSCDeliveryReceipt)
                {
                    //Get MessageId of delivered message
                    string messageId = data.Receipt.MessageId;
                    logger.Info("Delivered Receipt message id :" + messageId);
                    //MessageBox.Show(messageId, "Message ID");
                    MessageState deliveryStatus = data.Receipt.State;
                    
                    deliver_message = db.find_by_message_id(messageId);
                    logger.Info("Message " + deliver_message.ID + " sent to phone");
                    logger.Info("Message delivered info : " + data.ToString());
                    deliver_message.sent = "true";
                    deliver_message.Message_ID = "";
                    db.update_Message(deliver_message);
                   
                }
                else
                {

                    // Receive incoming message and try to concatenate all parts
                    if (data.Concatenation != null)
                    {

                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("" + ex, "An error occurred");
                data.Response.Status = CommandStatus.ESME_RX_T_APPN;
                logger.Error(" An error has occurred : ", ex);
                
            }
        }

        /// <summary>
        /// Function that detects succesfull bind operation
        /// (Debugging)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        public void client_evBindComplete(object sender, Inetlab.SMPP.PDU.BindResp data)
        {
            switch (data.Status)
            {
                case CommandStatus.ESME_ROK:
                    //MessageBox.Show("Bind result : system is " + data.SystemId + " with status " + data.Status.ToString(),"Bind result");
                    logger.Info("Bind to system : " + data.SystemId + "\nStatus : " + data.Status);
                    logger.Info("Bind info : " + data.ToString());
                    break;
                default:
                    //MessageBox.Show("Bad status returned during Bind : " + data.Command.ToString() + " with status " + data.Status.ToString(), "Bind result");
                    logger.Error("Bind to system : " + data.SystemId + "\nStatus : " + data.Status);
                    logger.Info("Bind info : " + data.ToString());
                    disconnect();
                    break;
                    
            }
        }
        
        public void client_evAlertNotifications(object sender, AlertNotification data)
        {
            logger.Info("Alert : " + data.ToString());
        }

        public void client_evConnected(object sender, bool success)
        {
            logger.Info("Connected status : " + success);
        }

        public void client_evDisconnected(object sender)
        {
            logger.Info("Disconnected status : " + sender.ToString());
        }

        public void client_evCancelComplete(object sender, CancelSmResp data)
        {
            logger.Info("Cancel done :" + data.ToString());
        }

        public void client_evUnbind(object sender, UnBind data)
        {
            logger.Info("Unbinded : " + data.ToString());
        }

        public void client_evSubmitComplete(object sender, SubmitSmResp data)
        {
            
            Broadcast_Message deliver_message;

            logger.Info("Message submitted Status : " + data.ToString());
            try
            {
                    Thread.Sleep(50);
                    string messageId = data.MessageId;
                    logger.Info("Submit Receipt Info :" + messageId);
                    deliver_message = db.find_by_message_id(messageId);
                    if( deliver_message.ID == 0)
                    {

                    }
                    else
                    {
                        deliver_message.submitted = "true";
                        db.update_Message(deliver_message);
                        logger.Info("Message "+ deliver_message.ID + " submitted to SMPP");
                    }
                logger.Info("Message Information : "
                   + "\nID :" + deliver_message.ID
                   + "\nDestination Address :" + deliver_message.Destination
                   + "\nMessage Text :" + deliver_message.Text
                   + "\nDate Send :" + deliver_message.Date_Time_Send
                 );

                logger.Info("Message submitted info : "
                                 +"\nMessage ID :" + data.MessageId
                                 +"\nStatus :" + data.Status
                                 +"\nClient :" + data.Client);
            }
            catch (Exception ex)
            {
                //MessageBox.Show("" + ex, "An error occurred");
                logger.Error(" An error has occurred : ", ex);
                
            }

        }

        public void client_evDeliverSmConplete(object sender, DeliverSm data)
        {
            logger.Info("Message delivered to phone : " + data.ToString());
        }

        public void client_evEnquireLink(object sender, EnquireLink data)
        {
            logger.Info("Enquire link notification : " + data.ToString());
        }

        public void client_evGenericNack(object sender, GenericNack data)
        {
            logger.Error("Generick nack returned : " + data.ToString());
        }

        public void client_evPduReceiving(object sender, SmppPDU data)
        {
            logger.Info("Pdu received : " + data.ToString());
        }

        public void client_evPduRejected(object sender, SmppPDU data)
        {
            logger.Warn("Pdu rejected : " + data.ToString());
        }

        public void client_evPduSending(object sender, SmppPDU data)
        {
            logger.Info(" Pdu sent : " + data.ToString());

        }

        public void Dispose()
        {
        }

    }
}
