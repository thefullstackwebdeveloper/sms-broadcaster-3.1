﻿using System;
using System.Windows.Forms;

namespace SMS_Broadcast_Message_Manager_v2.HelperClass
{
    class Validator
    {

        private TextBox temp;
        private Boolean empty;

        public Validator()
        {
                   
        }

        public Boolean textbox_Empty(TextBox sender)
        {
            empty = false;
            temp = (TextBox)sender;
            if (temp.Text == "")
            {
                empty = true;
            }
            return empty;
        }
    }
}
